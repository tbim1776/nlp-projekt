import re
import sys
import string
import pandas as pd
import tensorflow as tf

from os import path
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization


def custom_standardization(input_data):
    lowercase = tf.strings.lower(input_data)
    stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
    return tf.strings.regex_replace(stripped_html, '[%s]' % re.escape(string.punctuation), '')


def get_pairs(classes):
    return [tuple([classes[x], classes[y]]) for x in range(len(classes) - 1) for y in range(x + 1, len(classes))]


def create_dataframe(df, pair):
    temp_df = df[df['l2'].str.contains(pair[0])]
    return temp_df.append(df[df['l2'].str.contains(pair[1])])


def create_model(vocab_size, embedding_dim, train_ds):

    vectorize_layer = TextVectorization(
        standardize=custom_standardization,
        max_tokens=vocab_size,
        output_mode='int',
        output_sequence_length=embedding_dim)

    text_ds = train_ds.map(lambda x, y: x)
    vectorize_layer.adapt(text_ds)

    model = tf.keras.Sequential([
        vectorize_layer,
        tf.keras.layers.Embedding(vocab_size, embedding_dim),
        tf.keras.layers.GlobalAvgPool1D(),
        tf.keras.layers.Dense(embedding_dim, activation='relu'),
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                  metrics=['accuracy'])

    return model


def create_dataset(df, pair):
    target = df.pop('l2')
    target = target.replace(pair[0], 0)
    target = target.replace(pair[1], 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    train_dataset = dataset.shuffle(len(df)).batch(1)
    return train_dataset


def create_test_dataset(df, pair):
    target = df.pop('l2')
    target = target.replace(pair[0], 0)
    target = target.replace(pair[1], 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    test_dataset = dataset.batch(1)
    return test_dataset, target


def main(train, validation, test, vocab_size, embedding_dim):
    df_train = pd.read_csv(train, index_col=0)
    df_val = pd.read_csv(validation, index_col=0)
    df_test = pd.read_csv(test, index_col=0)

    unique_classes = df_train['l2'].unique()
    pairs = get_pairs(unique_classes)

    train_data = dict()
    validation_data = dict()
    test_data = dict()
    models = dict()
    predictions = dict()

    for pair in pairs:
        train_data[pair] = create_dataframe(df_train, pair)
        train_dataset = create_dataset(train_data[pair], pair)

        validation_data[pair] = create_dataframe(df_val, pair)
        validation_dataset = create_dataset(validation_data[pair], pair)

        test_data[pair] = create_dataframe(df_test, pair)
        test_dataset, y = create_test_dataset(test_data[pair], pair)

        models[pair] = create_model(vocab_size, embedding_dim, train_dataset)

        models[pair].fit(train_dataset, epochs=1, validation_data=validation_dataset)

        test_loss, test_acc = models[pair].evaluate(test_dataset)

        print('Evaluating ' + str(pair) + ': ' + str(test_acc * 100))

        # predictions = models[pair].predict(test_dataset)

        # if tf.round(predictions[0]) == y[0]:
        #    print('YEEEEEEEEEEEEEY')

        # models[pair].save('models/' + pair[0] + '_' + pair[1])

    # print('Models are saved!')

    df_test = pd.read_csv(test, index_col=0)
    target = df_test.pop('l2')
    dataset = tf.data.Dataset.from_tensor_slices(df_test.values)
    test_dataset = dataset.batch(1)

    for pair in pairs:
        predictions[pair] = models[pair].predict(test_dataset)

    correct = 0
    for i in range(len(predictions[pairs[0]])):
        chosen = dict()
        for c in unique_classes:
            chosen[c] = 0
        for pair in pairs:
            if tf.round(predictions[pair][i]) == 0:
                chosen[pair[0]] += 1
            else:
                chosen[pair[1]] += 1
        max_key = max(chosen, key=chosen.get)
        if max_key == target[i]:
            correct += 1

    print('Precision of one versus one method: ' + str((correct/len(predictions[pairs[0]]))*100))


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('Usage: ./create_models.py </path/to/train/filename> </path/to/validation/filename> '
              '</path/to/test/filename> <vocab_size> <embedding_dim>')
        exit()
    if path.exists(sys.argv[1]) and path.exists(sys.argv[2]) and path.exists(sys.argv[3]):
        main(sys.argv[1], sys.argv[2], sys.argv[3], int(sys.argv[4]), int(sys.argv[5]))
    else:
        print('File not found!')
        exit()
