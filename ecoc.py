import re
import sys
import string
import pandas as pd
import tensorflow as tf

from os import path
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization


def custom_standardization(input_data):
    lowercase = tf.strings.lower(input_data)
    stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
    return tf.strings.regex_replace(stripped_html, '[%s]' % re.escape(string.punctuation), '')


def get_pairs(classes):
    pairs = [tuple([['Athlete'], ['Company', 'Person', 'Politician']]),
             tuple([['Athlete', 'Politician'], ['Company', 'Person']]),
             tuple([['Athlete', 'Person'], ['Company', 'Politician']]),
             tuple([['Athlete', 'Person', 'Politician'], ['Company']]),
             tuple([['Athlete', 'Company'], ['Person', 'Politician']]),
             tuple([['Athlete', 'Company', 'Politician'], ['Person']]),
             tuple([['Athlete', 'Company', 'Person'], ['Politician']])]
    return pairs


def hamming(df, pairs):
    class_dis = [
        [1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 1, 1, 1],
        [0, 0, 1, 1, 0, 0, 1],
        [0, 1, 0, 1, 0, 1, 0]
    ]

    dist = dict()
    for i in range(4):
        distance = 0
        for index, pair in enumerate(pairs):
            distance += abs(df[repr(pair)] - class_dis[i][index])
        dist[i] = distance
    return max(dist, key=dist.get)


def create_model(vocab_size, embedding_dim, train_ds):
    vectorize_layer = TextVectorization(
        standardize=custom_standardization,
        max_tokens=vocab_size,
        output_mode='int',
        output_sequence_length=embedding_dim)

    text_ds = train_ds.map(lambda x, y: x)
    vectorize_layer.adapt(text_ds)

    model = tf.keras.Sequential([
        vectorize_layer,
        tf.keras.layers.Embedding(vocab_size, embedding_dim),
        tf.keras.layers.GlobalAvgPool1D(),
        tf.keras.layers.Dense(embedding_dim, activation='relu'),
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                  metrics=['accuracy'])

    return model


def create_dataset(df, pair):
    target = df.pop('l2')
    for p in pair[0]:
        target = target.replace(p, 0)
    for p in pair[1]:
        target = target.replace(p, 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    train_dataset = dataset.shuffle(len(df)).batch(1)
    return train_dataset


def create_test_dataset(df, pair):
    target = df.pop('l2')
    for p in pair[0]:
        target = target.replace(p, 0)
    for p in pair[1]:
        target = target.replace(p, 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    test_dataset = dataset.batch(1)
    return test_dataset, target


def main(train, validation, test, vocab_size, embedding_dim):
    df_train = pd.read_csv(train, index_col=0)
    df_val = pd.read_csv(validation, index_col=0)
    df_test = pd.read_csv(test, index_col=0)

    unique_classes = df_train['l2'].unique()
    pairs = get_pairs(unique_classes)

    models = dict()
    predictions = dict()

    for pair in pairs:
        train_dataset = create_dataset(df_train, pair)

        validation_dataset = create_dataset(df_val, pair)

        test_dataset, y = create_test_dataset(df_test, pair)

        models[repr(pair)] = create_model(vocab_size, embedding_dim, train_dataset)

        models[repr(pair)].fit(train_dataset, epochs=1, validation_data=validation_dataset)

        test_loss, test_acc = models[repr(pair)].evaluate(test_dataset)

        print('Evaluating ' + str(pair) + ': ' + str(test_acc * 100))

        df_train = pd.read_csv(train, index_col=0)
        df_val = pd.read_csv(validation, index_col=0)
        df_test = pd.read_csv(test, index_col=0)

    df_test = pd.read_csv(test, index_col=0)
    target = df_test.pop('l2')
    dataset = tf.data.Dataset.from_tensor_slices(df_test.values)
    test_dataset = dataset.batch(1)

    for pair in pairs:
        predictions[repr(pair)] = models[repr(pair)].predict(test_dataset)

    correct = 0
    for i in range(len(predictions[repr(pairs[0])])):
        chosen = dict()
        for pair in pairs:
            if tf.round(predictions[repr(pair)][i]) == 0:
                chosen[repr(pair)] = 0
            else:
                chosen[repr(pair)] = 1

        max_key = hamming(chosen, pairs)
        # print(max_key, target[i])
        if unique_classes[max_key] == target[i]:
            correct += 1

    print('Precision of ECOC method: ' + str((correct / len(predictions[repr(pairs[0])])) * 100))


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('Usage: ./create_models.py </path/to/train/filename> </path/to/validation/filename> '
              '</path/to/test/filename> <vocab_size> <embedding_dim>')
        exit()
    if path.exists(sys.argv[1]) and path.exists(sys.argv[2]) and path.exists(sys.argv[3]):
        main(sys.argv[1], sys.argv[2], sys.argv[3], int(sys.argv[4]), int(sys.argv[5]))
    else:
        print('File not found!')
        exit()
