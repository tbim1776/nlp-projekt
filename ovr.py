import re
import sys
import string
import pandas as pd
import tensorflow as tf

from os import path
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization


def custom_standardization(input_data):
    lowercase = tf.strings.lower(input_data)
    stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
    return tf.strings.regex_replace(stripped_html, '[%s]' % re.escape(string.punctuation), '')


def get_pairs(classes):
    return [tuple([classes[x], classes[y]]) for x in range(len(classes) - 1) for y in range(x + 1, len(classes))]


def create_dataframe(df, c):
    temp_df = df[df['l2'].str.contains(c)]
    l = len(temp_df)/3
    classes = df['l2'].unique()
    for clas in classes:
        if clas != c:
            temp_df = temp_df.append((df[df['l2'].str.contains(clas)]).head(int(l)))
    return temp_df


def create_model(vocab_size, embedding_dim, train_ds):

    vectorize_layer = TextVectorization(
        standardize=custom_standardization,
        max_tokens=vocab_size,
        output_mode='int',
        output_sequence_length=embedding_dim)

    text_ds = train_ds.map(lambda x, y: x)
    vectorize_layer.adapt(text_ds)

    model = tf.keras.Sequential([
        vectorize_layer,
        tf.keras.layers.Embedding(vocab_size, embedding_dim),
        tf.keras.layers.GlobalAvgPool1D(),
        tf.keras.layers.Dense(embedding_dim, activation='relu'),
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                  metrics=['accuracy'])

    return model


def create_dataset(df, c):
    target = df.pop('l2')
    unique_classes = target.unique()
    target = target.replace(c, 0)
    for cl in unique_classes:
        if cl != c:
            target = target.replace(cl, 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    train_dataset = dataset.shuffle(len(df)).batch(1)
    return train_dataset


def create_test_dataset(df, c):
    target = df.pop('l2')
    unique_classes = target.unique()
    target = target.replace(c, 0)
    for cl in unique_classes:
        if cl != c:
            target = target.replace(cl, 1)
    dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
    test_dataset = dataset.batch(1)
    return test_dataset, target


def main(train, validation, test, vocab_size, embedding_dim):
    df_train = pd.read_csv(train, index_col=0)
    df_val = pd.read_csv(validation, index_col=0)
    df_test = pd.read_csv(test, index_col=0)

    unique_classes = df_train['l2'].unique()
    print(unique_classes)

    train_data = dict()
    validation_data = dict()
    test_data = dict()
    models = dict()
    predictions = dict()

    for c in unique_classes:
        train_data[c] = create_dataframe(df_train, c)
        train_dataset = create_dataset(train_data[c], c)

        validation_data[c] = create_dataframe(df_val, c)
        validation_dataset = create_dataset(validation_data[c], c)

        test_data[c] = create_dataframe(df_test, c)
        test_dataset, y = create_test_dataset(test_data[c], c)

        models[c] = create_model(vocab_size, embedding_dim, train_dataset)

        models[c].fit(train_dataset, epochs=1, validation_data=validation_dataset)

        test_loss, test_acc = models[c].evaluate(test_dataset)

        print('Evaluating ' + str(c) + ': ' + str(test_acc * 100))

        # predictions = models[pair].predict(test_dataset)

        # if tf.round(predictions[0]) == y[0]:
        #    print('YEEEEEEEEEEEEEY')

        # models[pair].save('models/' + pair[0] + '_' + pair[1])

    # print('Models are saved!')

    df_test = pd.read_csv(test, index_col=0)
    target = df_test.pop('l2')
    dataset = tf.data.Dataset.from_tensor_slices(df_test.values)
    test_dataset = dataset.batch(1)

    for cl in unique_classes:
        predictions[cl] = models[cl].predict(test_dataset)

    correct = 0
    for i in range(len(predictions[unique_classes[0]])):
        chosen = dict()
        for cl in unique_classes:
            chosen[cl] = 1 - predictions[cl][i]
        max_key = max(chosen, key=chosen.get)
        if max_key == target[i]:
            correct += 1

    print('Precision of one versus rest method: ' + str((correct/len(predictions[unique_classes[0]]))*100))


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('Usage: ./create_models.py </path/to/train/filename> </path/to/validation/filename> '
              '</path/to/test/filename> <vocab_size> <embedding_dim>')
        exit()
    if path.exists(sys.argv[1]) and path.exists(sys.argv[2]) and path.exists(sys.argv[3]):
        main(sys.argv[1], sys.argv[2], sys.argv[3], int(sys.argv[4]), int(sys.argv[5]))
    else:
        print('File not found!')
        exit()
